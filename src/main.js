// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import qs from 'qs'
import AMap from 'AMap'
import AmazeVue from 'amaze-vue'
// import VueScroller from 'vue-scroller'
import MyMain from './components/mine/myMain.vue'
import Footer from './components/public/footer.vue'
import Banner from './components/public/banner.vue'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import Impression from 'vue-impression'
import Search from './components/public/search.vue'
import TextPop from './components/public/textPop.vue'
import Global from './components/public/global.vue'
import CityPicker from './components/public/cityPicker.vue'
import Gohome from './components/public/goHome.vue'
import vuex from 'vuex'
import store from './vuex/index'

// Vue.prototype.store = store;


import './assets/Public/wangwen/css/demo.1.css';
import 'swiper/dist/css/swiper.min.css';
import 'amaze-vue/dist/amaze-vue.css';

axios.defaults.withCredentials = true




Vue.component('myMain',MyMain);
Vue.component('Footer',Footer);
Vue.component('Banner',Banner);
Vue.component('Search',Search);
Vue.component('TextPop',TextPop);
Vue.component('city-picker',CityPicker);
Vue.component('go-home',Gohome);





Vue.use(VueAwesomeSwiper)
Vue.use(Impression)
Vue.use(qs)
Vue.use(Global)
Vue.use(AMap)
Vue.use(AmazeVue)
Vue.use(vuex)
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
Vue.prototype.$http=axios
let bus = new Vue()
Vue.prototype.bus = bus

Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
