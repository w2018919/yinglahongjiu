import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const store = new Vuex.Store({
  state:{
    city:'',
    lat:'',
    lng:'',
    search:'',
    addressComponent:'',
    agentFilter:'',
  },
  getters:{
    getCity(state){
      return state.city;
    },
    getLatLng(state){
      return state.lat+','+state.lng
    },
    getAddressComponent(state){
      return state.addressComponent;
    },
    getAgentFilter(state){
      return state.gender +','+state.age
    },
  },
  mutations:{
    'SET_CITY'(state,city){
      state.city = city;
    },
    'SET_LatLng'(state,latlng){
      state.lat=latlng[0];
      state.lng=latlng[1];
    },
    'SET_AddressComponent'(state,addressComponent){
      state.addressComponent=addressComponent
    },
    'SET_SFilter'(state,filter){
      state.gender=filter[0];
      state.age=filter[1];
    }
  },

  actions:{
    setCity({commit},city){
      commit('SET_CITY',city)
    },
    setLatLng({commit},latlng){
      commit('SET_LatLng',latlng)
    },
    setAddressComponent({commit},addressComponent){
      commit("SET_AddressComponent",addressComponent)
    },
    setSFilter({commit},filter){
      commit('SET_SFilter',filter)
    },
  }

})
export default store;
