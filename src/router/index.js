import Vue from 'vue'
import VueRouter from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
import SommeliersIndex from '@/components/sommeliers/sommeliersIndex'
import SommeliersSort from '@/components/sommeliers/sommeliersSort'
import SommelierDetail from '@/components/sommeliers/sommelierDetail'
import SommelierAward from '@/components/sommeliers/sommelierAward'
import ThanksLetter from '@/components/sommeliers/thanksLetter'
import Shop from '@/components/shop/shopIndex'
import GoodsDetail from '@/components/shop/goodsDetail'
import UserComments from '@/components/shop/userComments'
import BuyNow from '@/components/shop/buyNow'
import EmptyPage from '@/components/shop/emptyPage'
import Cart from '@/components/shoppingCart/cartIndex'
import MyMain from '@/components/mine/myMain'
import MyOrder from '@/components/mine/myOrder'
import OrderDetail from '@/components/mine/orderDetail'
import Evaluate from '@/components/mine/evaluate'
import ModifyPersonal from '@/components/mine/modifyPersonal'
import MyBusiness from '@/components/mine/myBusiness'
import OrderTaking from '@/components/mine/orderTaking'
import RegisterMember from '@/components/mine/registerMember'
import MyAddress from '@/components/mine/address/myAddress'
import AddAddress from '@/components/mine/address/addAddress'

import Upgrade from '@/components/mine/upgrade'

import Profile from '@/components/mine/profile'
import Apply from '@/components/mine/apply'
import Newly from '@/components/mine/newly'
import Contact from '@/components/mine/contact'
import Ranking from '@/components/mine/ranking'
import MyWallet from '@/components/mine/myWallet'
import TransactionDetail from  '@/components/mine/transactionDetail'
import MyTeam from '@/components/mine/myTeam'
import MyReservation from '@/components/mine/MyReservation'
import Purchase from '@/components/mine/Purchase'
import PurchaseCart from '@/components/mine/purchaseCart'
import MyStock from '@/components/mine/MyStock'
import MyDetailed from '@/components/mine/MyDetailed'
import Invitation from '@/components/mine/Invitation'
Vue.use(VueRouter)

export default new VueRouter({
  // mode:"history",
  base:"/dist/",
  routes: [
    {
      path: '/', //首页 侍酒师
      name: 'sommeliersIndex',
      component: SommeliersIndex,
      meta:{keepAlive:true}
    },
    {
      path: '/sommeliersSort', //侍酒师排名列表页面
      name: 'sommeliersSort',
      component: SommeliersSort
    },
    {
      path: '/sommelierDetail',//侍酒师详情
      name: 'sommelierDetail',
      component: SommelierDetail
    },
    {
      path: '/sommelierAward', //侍酒师打赏页面
      name: 'sommelierAward',
      component: SommelierAward
    },
    {
      path: '/thanksLetter', //感谢函
      name: 'thanksLetter',
      component: ThanksLetter
    },
    {
      path: '/shop', //商城
      name: 'shop',
      component: Shop
    },
    {
      path: '/emptyPage', //空白页面
      name: 'emptyPage',
      component: EmptyPage
    },
    {
      path: '/goodsDetail', //商品详情部分
      name: 'goodsDetail',
      component: GoodsDetail
    },
    {
      path: '/buyNow', //立即购买页面
      name: 'buyNow',
      component: BuyNow,
      meta:{
        keepAlive:true
      }
    },
    {
      path: '/usercomments', //用户评论
      name: 'UserComments',
      component: UserComments
    },
    {
      path: '/cart', //购物车
      name: 'cart',
      component: Cart
    },
    {
      path: '/mine',//我的
      name: 'myMain',
      component: MyMain
    },
    {
      path: '/myOrder',//我的  我的订单
      name: 'myOrder',
      component: MyOrder
    },
    {
      path: '/orderDetail',// 订单详情
      name: 'orderDetail',
      component: OrderDetail
    },
    {
      path: '/evaluate', //评价订单
      name: 'evaluate',
      component: Evaluate
    },
    {
      path: '/modifyPersonal',//修改个人信息
      name: 'modifyPersonal',
      component: ModifyPersonal
    },
    {
      path: '/myBusiness',//我的业务
      name: 'myBusiness',
      component: MyBusiness
    },
    {
      path: '/orderTaking',//是否接单
      name: 'orderTaking',
      component: OrderTaking
    },
    {
      path: '/myAddress',//我的地址
      name: 'myAddress',
      component: MyAddress
    },
    {
      path: '/addAddress',//添加地址
      name: 'addAddress',
      component: AddAddress
    },

    {
      path: '/newly',//新建邀请函
      name: 'newly',
      component: Newly
    },
    {
      path: '/upgrade',//我要升级
      name: 'upgrade',
      component: Upgrade
    },
    {
      path: '/profile',//公司简介
      name: 'profile',
      component: Profile
    },
    {
      path: '/apply',//申请加盟区域
      name: 'apply',
      component: Apply
    },
    {
      path: '/contact',//联系我们
      name: 'contact',
      component: Contact
    },
    {
      path: '/ranking',//我的 排名
      name: 'ranking',
      component: Ranking
    },
    {
      path: '/myWallet',//我的钱包
      name: 'myWallet',
      component: MyWallet
    },
    {
      path: '/transactionDetail',//我的钱包
      name: 'transactionDetail',
      component: TransactionDetail
    },
    {
      path: '/myTeam',//我的团队
      name: 'myTeam',
      component: MyTeam
    },
    {
      path: '/myReservation',//我的预约
      name: 'myReservation',
      component: MyReservation
    },
    {
      path: '/Purchase',//进货-补货
      name: 'Purchase',
      component: Purchase
    },
    {
      path: '/purchaseCart', //进货购物车
      name: 'purchaseCart',
      component: PurchaseCart
    },
    {
      path: '/myStock',//我的库存
      name: 'myStock',
      component: MyStock
    },
    {
      path: '/myDetailed',//出入库明细
      name: 'myDetailed',
      component: MyDetailed
    },
    {
      path: '/invitation',//邀请函
      name: 'invitation',
      component: Invitation
    },
    {
      path: '/registerMember',//邀请函
      name: 'registerMember',
      component: RegisterMember
    },
    {
      path: '*', //404页面
      component:(resolve)=>require(['@/components/public/error404.vue'],resolve)
    },
    // {
    //   path: '/test', //404页面
    //   component:(resolve)=>require(['@/components/public/vueCropper.vue'],resolve)
    //   // component:(resolve)=>require(['@/components/public/test.vue'],resolve)
    // },
  ]
})
